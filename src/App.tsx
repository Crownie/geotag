import React, {useMemo} from 'react';
import './App.css';
import Routes from './app/Routes';
import {DrawerMenuProvider} from './common/components/DrawerMenu';
import {ThemeProvider} from 'styled-components';
import {
  createMuiTheme,
  ThemeProvider as MaterialThemeProvider,
} from '@material-ui/core/styles';
import {ToastProvider} from './common/components/Toast';
import PWAInstallProvider from './features/pwa/PWAInstallProvider';

function App() {
  const theme = useMemo(() => createMuiTheme(), []);
  return (
    <MaterialThemeProvider theme={theme}>
      <ThemeProvider theme={theme}>
        <ToastProvider>
          <DrawerMenuProvider>
            <PWAInstallProvider>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  width: '100%',
                  position: 'fixed',
                  top: 0,
                  bottom: 0,
                }}
              >
                <Routes />
              </div>
            </PWAInstallProvider>
          </DrawerMenuProvider>
        </ToastProvider>
      </ThemeProvider>
    </MaterialThemeProvider>
  );
}

export default App;
