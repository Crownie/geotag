export class LocationService {
  getLocation(): Promise<Position> {
    if(!navigator.geolocation){
      const msg = 'Geolocation is not supported by your browser. Please try a different browser.'
      alert(msg);
      throw new Error(msg);
    }
    // return {coords:{longitude:3.379206,latitude:6.524379}} as any;
    return new Promise((resolve, reject) => {
      console.log('requesting location...');
      navigator.geolocation.getCurrentPosition((position) => {
        // convert the received position to a normal object
        // the received `position` is not a serializable object
        const pos:Position = {
          timestamp:position.timestamp,
          coords:{
            latitude:position.coords.latitude,
            longitude:position.coords.longitude,
            accuracy:position.coords.accuracy
          } as any
        }
        resolve(pos);
      }, (error) => {
        reject(error);
      },{
        enableHighAccuracy: true,
        timeout: 10000,
        maximumAge: 0
      });
    })
  }
}
