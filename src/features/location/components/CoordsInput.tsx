import React, {FunctionComponent, useCallback, useRef} from 'react';
import {IconButton, InputAdornment, TextField, TextFieldProps} from '@material-ui/core';
import {FileCopyOutlined} from '@material-ui/icons';
import {toast} from '../../../common/components/Toast';



const CoordsInput: FunctionComponent<TextFieldProps> = ({value, ...props}) => {

  const inputRef = useRef<HTMLInputElement>(null);

  const handleCopy = useCallback(() => {
    // store disabled
    const disabledState = inputRef.current!.disabled;
    inputRef.current!.disabled = false;
    inputRef.current!.select();
    inputRef.current!.setSelectionRange(0, 99999); /* For mobile devices */
    /* Copy the text inside the text field */
    document.execCommand("copy");
    // restore previous disabled
    inputRef.current!.disabled = disabledState;
    inputRef.current!.blur();
    toast('info', 'Copied!')
  }, []);

  return <TextField
    {...props}
    inputRef={inputRef}
    variant="outlined"
    fullWidth
    value={value || ''}
    InputProps={{
      endAdornment: <InputAdornment position="end">
        <IconButton onClick={handleCopy}>
          <FileCopyOutlined/>
        </IconButton>
      </InputAdornment>
    }}/>;
};

export default CoordsInput;
