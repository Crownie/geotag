import React, {FunctionComponent, useCallback, useState} from 'react';
import {
  Button,
  DialogContent,
  InputAdornment,
  TextField,
  Typography,
} from '@material-ui/core';
import {CloudDownload, InsertDriveFileOutlined} from '@material-ui/icons';
import {useSelector} from 'react-redux';
import {selectLocations} from '../location.slice';
import {
  convertArrayOfObjectsToCSV,
  downloadCSV,
} from '../../../common/utils/csv';
import {Box} from '../../../common/components/Box';
import DialogTitle from '../../../common/components/DialogTitle';
import {useModalContext} from 'react-modal-helper';

interface Props {}

const ExportCsvModal: FunctionComponent<Props> = () => {
  const {close} = useModalContext();
  const [name, setName] = useState<string>('geo-coordinates');

  const locations = useSelector(selectLocations);
  const handleDownload = useCallback(() => {
    const rows = locations.map((location) => {
      return {
        name: location.name,
        latitude: location.position.coords.latitude,
        longitude: location.position.coords.longitude,
      };
    });
    const csv = convertArrayOfObjectsToCSV(rows);
    const n = name.includes('.csv') ? name : name + '.csv';
    downloadCSV(csv, n);
  }, [locations, name]);
  return (
    <>
      <DialogTitle onClose={() => close()}>Export CSV file</DialogTitle>
      <DialogContent>
        <Typography variant="caption">Download a csv file for Excel</Typography>
        <Box mt={3} />
        <TextField
          variant="outlined"
          label="Filename"
          fullWidth
          value={name}
          onChange={(e) => setName(e.target.value)}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <InsertDriveFileOutlined />
              </InputAdornment>
            ),
            endAdornment: <InputAdornment position="end">.csv</InputAdornment>,
          }}
        />
        <Box pv={3} justify="center">
          <Button
            variant="contained"
            color="primary"
            onClick={handleDownload}
            startIcon={<CloudDownload />}
            fullWidth
          >
            Download
          </Button>
        </Box>
      </DialogContent>
    </>
  );
};

export default ExportCsvModal;
