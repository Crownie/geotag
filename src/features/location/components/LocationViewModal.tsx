import React, {FunctionComponent, useCallback, useState} from 'react';
import Map from './Map';
import {Location, locationUpdated} from '../location.slice';
import DialogContent from '@material-ui/core/DialogContent';
import {Box, Button, DialogActions, TextField} from '@material-ui/core';
import {useModalContext} from 'react-modal-helper';
import LatLngInputs from './LatLngInputs';
import {useDispatch} from 'react-redux';
import {toast} from '../../../common/components/Toast';

interface Props {
  location: Location
}

const LocationViewModal: FunctionComponent<Props> = ({location}) => {
  const {close} = useModalContext();
  const dispatch = useDispatch();
  const [name, setName] = useState<string>(location.name);
  const [coords, setCoords] = useState(location.position.coords);
  const {latitude, longitude} = coords;

  const handleCoordsChange = useCallback((val) => {
    setCoords(val)
  }, []);

  const handleSubmit = useCallback(() => {
    const position = {coords} as any;
    dispatch(locationUpdated({...location, name, position}))
    toast('success', 'Location was updated successfully')
    close();
  }, [close, coords, dispatch, location, name]);

  return <>
    <TextField variant="filled" label="Name" value={name} onChange={e => setName(e.target.value)}/>
    <DialogContent>
      <Box pb={2}/>
      <LatLngInputs coords={coords} small/>
      <Box pb={2}/>
      <Map latitude={latitude} longitude={longitude} onChange={handleCoordsChange}/>
    </DialogContent>
    <DialogActions>
      <Button onClick={() => close()}>Cancel</Button>
      <Button color="primary" onClick={handleSubmit}>Update</Button>
    </DialogActions>
  </>;
};

export default LocationViewModal;
