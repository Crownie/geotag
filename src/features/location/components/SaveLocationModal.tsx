import React, {FunctionComponent, useCallback, useState} from 'react';
import {locationAdded, selectCurrentLocation} from '../location.slice';
import {useDispatch, useSelector} from 'react-redux';
import {Button, DialogActions, DialogContent, Grid, TextField} from '@material-ui/core';
import {useModalContext} from 'react-modal-helper';
import DialogTitle from '../../../common/components/DialogTitle';
import {toast} from '../../../common/components/Toast';

interface Props {
}

const SaveLocationModal: FunctionComponent<Props> = () => {
  const dispatch = useDispatch();
  const position = useSelector(selectCurrentLocation);
  const {close} = useModalContext();
  const [name, setName] = useState<string>('');


  const handleSubmit = useCallback((event) => {
    event.preventDefault();
    if (!position) {
      return;
    }
    const n = name.trim();
    if (!n) {
      alert('Name is required!');
      return;
    }
    dispatch(locationAdded({name: n, position}));
    toast('success', 'Location was saved successfully')
    close();
  }, [dispatch, name, position, close]);

  const handleClose = useCallback(() => {
    close()
  }, [close]);

  return <>
    <DialogTitle onClose={handleClose}>Save location</DialogTitle>
    <DialogContent>
      <form action="" onSubmit={handleSubmit}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField onChange={e => setName(e.target.value)} variant="outlined" label="Name" required fullWidth/>
          </Grid>
          <Grid item xs={12}>
            <Button variant="contained" color="primary" type="submit" fullWidth>Submit</Button>
          </Grid>
        </Grid>
      </form>
    </DialogContent>
    <DialogActions/>
  </>;
};

export default SaveLocationModal;
