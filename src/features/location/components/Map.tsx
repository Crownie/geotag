import React, {FunctionComponent, useCallback, useEffect} from 'react';
import {MapContainer, Marker, TileLayer, useMap, Tooltip} from 'react-leaflet';
import {DragEndEvent} from 'leaflet';

interface Props {
  latitude: number;
  longitude: number;
  onChange?: (coords: {latitude: number; longitude: number}) => void;
  zoom?: number;
}

const Map: FunctionComponent<Props> = ({
  latitude,
  longitude,
  onChange,
  zoom = 17,
}) => {
  const handleDragEnd = useCallback(
    (event: DragEndEvent) => {
      const {lat, lng} = event.target._latlng;
      onChange && onChange({latitude: lat, longitude: lng});
    },
    [onChange],
  );

  useEffect(() => {});

  return (
    <MapContainer
      style={{height: 300}}
      center={[latitude, longitude]}
      zoom={zoom}
      scrollWheelZoom={false}
    >
      <ChangeView center={[latitude, longitude]} zoom={zoom} />
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker
        draggable={true}
        eventHandlers={{
          dragend: handleDragEnd,
        }}
        position={[latitude, longitude]}
      >
        <Tooltip>This marker is draggable</Tooltip>
      </Marker>
    </MapContainer>
  );
};

const ChangeView: FunctionComponent<any> = ({center}) => {
  const map = useMap();
  map.setView(center, map.getZoom());
  return null;
};

export default Map;
