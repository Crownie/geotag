import React, {FunctionComponent} from 'react';
import {Grid} from '@material-ui/core';
import {useSelector} from 'react-redux';
import {selectCurrentLocation} from '../location.slice';
import CoordsInput from './CoordsInput';

interface Props {
  coords?: Coordinates
  small?:boolean;
}

const LatLngInputs: FunctionComponent<Props> = ({coords,small}) => {
  const position = useSelector(selectCurrentLocation);
  coords = coords || position?.coords;
  return <Grid container spacing={2}>
    <Grid item xs={12}><CoordsInput label="Latitude"
                                    value={coords?.latitude} disabled size={small?'small':undefined}/></Grid>
    <Grid item xs={12}> <CoordsInput label="Longitude"
                                     value={coords?.longitude} disabled size={small?'small':undefined}/>
    </Grid>
  </Grid>;
};

export default LatLngInputs;
