import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {v4 as uuid} from 'uuid';
import {LocationService} from './location.service';
import {RootState} from '../../app/reducer';

const locationService = new LocationService();

export const fetchLocation = createAsyncThunk<Position>(
  'location/fetch',
  (d, {dispatch}) => {
    return locationService.getLocation();
  },
);

export interface Coordinates {
  latitude: number;
  longitude: number;
}

export interface Location {
  id?: string;
  name: string;
  position: Position;
}

interface LocationState {
  current?: Position;
  fetching: boolean;
  locations: Location[];
  error?: {
    name: string;
    message: string;
  };
}

const initialState: LocationState = {
  current: undefined,
  fetching: false,
  error: undefined,
  locations: [],
};
const {reducer: locationReducer, actions: locationActions} = createSlice({
  name: 'location',
  initialState,
  reducers: {
    coordinatesUpdated: (state, {payload}: PayloadAction<Coordinates>) => {
      const {longitude, latitude} = payload;
      if (!state.current) {
        state.current = {coords: {}} as any;
      }
      if (state.current) {
        state.current.coords = {
          ...state.current.coords,
          longitude,
          latitude,
        } as any;
      }
    },
    locationAdded: (state, {payload}: PayloadAction<Location>) => {
      const existingIndex = state.locations.findIndex(({position}) => {
        const {latitude: lat1, longitude: lng1} = payload.position.coords;
        return (
          lat1 === position.coords.latitude &&
          lng1 === position.coords.longitude
        );
      });
      if (existingIndex >= 0) {
        state.locations[existingIndex] = payload;
      } else {
        state.locations.unshift({...payload, id: uuid()});
      }
    },
    locationUpdated: (state, {payload}: PayloadAction<Location>) => {
      const existingIndex = state.locations.findIndex(({id}) => {
        return payload.id === id;
      });
      if (existingIndex >= 0) {
        state.locations[existingIndex] = payload;
      }
    },
    locationDeleted: (state, {payload}: PayloadAction<string>) => {
      const existingIndex = state.locations.findIndex(({id}) => {
        return payload === id;
      });
      if (existingIndex >= 0) {
        state.locations.splice(existingIndex, 1);
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchLocation.pending, (state) => {
        state.fetching = true;
      })
      .addCase(
        fetchLocation.fulfilled,
        (state, {payload}: PayloadAction<Position>) => {
          state.current = payload;
          state.fetching = false;
        },
      )
      .addCase(fetchLocation.rejected, (state, {error}: any) => {
        state.error = error;
        state.fetching = false;
      });
  },
});

export const {
  coordinatesUpdated,
  locationAdded,
  locationUpdated,
  locationDeleted,
} = locationActions;

export const selectCurrentLocation = (state: RootState) =>
  state.location.current;
export const selectLocations = (state: RootState) => state.location.locations;
export const selectFilterLocations = (name: string) => (state: RootState) =>
  state.location.locations.filter(
    (location) => location.name.indexOf(name) >= 0,
  );

export default locationReducer;
