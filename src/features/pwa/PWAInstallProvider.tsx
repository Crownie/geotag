import React, {
  FunctionComponent,
  useCallback,
  useContext,
  useMemo,
} from 'react';
import {useModal} from 'react-modal-helper';
import MyModalAdapter from '../../common/components/MyModalAdapter';
import {useA2HSPrompt} from './useA2HSPrompt';
import {isIOS} from '../../common/utils/device';
import InstallationGuideModalIOS from './InstallationGuideModalIOS';

interface PWAInstallContextProps {
  available?: boolean;
  openPrompt: () => void;
}

const PWAInstallContext = React.createContext<PWAInstallContextProps>(
  {} as any,
);

const PWAInstallProvider: FunctionComponent = ({children}) => {
  const {open, ModalRenderer} = useModal(MyModalAdapter);
  const [prompt, promptToInstall] = useA2HSPrompt();

  const openPrompt = useCallback(() => {
    if (isIOS()) {
      open(<InstallationGuideModalIOS />);
    } else {
      promptToInstall();
    }
  }, [open, promptToInstall]);

  const available = (!!prompt || isIOS()) && getPWADisplayMode() === 'browser';

  const value = useMemo(() => ({openPrompt, available}), [
    openPrompt,
    available,
  ]);

  return (
    <PWAInstallContext.Provider value={value}>
      <ModalRenderer />
      {children}
    </PWAInstallContext.Provider>
  );
};

export const usePWAInstall = () => {
  return useContext(PWAInstallContext);
};

function getPWADisplayMode() {
  const isStandalone = window.matchMedia('(display-mode: standalone)').matches;
  if (document.referrer.startsWith('android-app://')) {
    return 'twa';
  } else if ((navigator as any).standalone || isStandalone) {
    return 'standalone';
  }
  return 'browser';
}

export default PWAInstallProvider;
