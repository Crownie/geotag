import React, {FunctionComponent} from 'react';
import {
  Avatar,
  Button,
  DialogActions,
  DialogContent,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
} from '@material-ui/core';
import DialogTitle from '../../common/components/DialogTitle';
import {useModalContext} from 'react-modal-helper';

interface Props {}

const InstallationGuideModalIOS: FunctionComponent<Props> = () => {
  const {close} = useModalContext();
  return (
    <>
      <DialogTitle>Follow these steps (iPhone)</DialogTitle>
      <DialogContent>
        <List style={{width: '100%'}}>
          <ListItem>
            <ListItemAvatar>
              <Avatar>1</Avatar>
            </ListItemAvatar>
            <ListItemText primary="Click 'Share'" />
          </ListItem>
          <ListItem>
            <ListItemAvatar>
              <Avatar>2</Avatar>
            </ListItemAvatar>
            <ListItemText primary="Then click 'Add to Home Screen'" />
          </ListItem>
        </List>
      </DialogContent>
      <DialogActions>
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            close();
          }}
          fullWidth
        >
          Done
        </Button>
      </DialogActions>
    </>
  );
};

export default InstallationGuideModalIOS;
