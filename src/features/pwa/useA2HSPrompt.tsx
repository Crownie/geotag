import React from 'react';

interface IBeforeInstallPromptEvent extends Event {
  readonly platforms: string[];
  readonly userChoice: Promise<{
    outcome: 'accepted' | 'dismissed';
    platform: string;
  }>;

  prompt(): Promise<void>;
}

export function useA2HSPrompt(): [
  IBeforeInstallPromptEvent | null,
  () => void,
] {
  const [prompt, setState] = React.useState<IBeforeInstallPromptEvent | null>(
    null,
  );

  const promptToInstall = async () => {
    if (prompt) {
      await prompt.prompt();
    }
  };

  React.useEffect(() => {
    console.log('register event');

    const ready = (e: IBeforeInstallPromptEvent) => {
      console.log('Fired!!!');
      e.preventDefault();
      setState(e);
    };

    window.addEventListener('beforeinstallprompt', ready as any);

    return () => {
      window.removeEventListener('beforeinstallprompt', ready as any);
    };
  }, []);

  return [prompt, promptToInstall];
}
