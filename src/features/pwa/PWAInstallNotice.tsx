import React, {FunctionComponent} from 'react';
import {Button, Typography} from '@material-ui/core';
import {Box} from '../../common/components/Box';
import {usePWAInstall} from './PWAInstallProvider';
import {logEvent} from '../../common/utils/analytics';

interface Props {}

const PWAInstallNotice: FunctionComponent<Props> = () => {
  const {available, openPrompt} = usePWAInstall();
  return (
    <>
      {available && (
        <Box direction="column" p={2}>
          <Typography variant="caption">
            This app works offline. Click install to add it to your Home screen
            for easier access.
          </Typography>
          <Button
            variant="outlined"
            color="secondary"
            onClick={() => {
              logEvent('click', 'Install App');
              openPrompt();
            }}
            fullWidth
          >
            Install App
          </Button>
        </Box>
      )}
    </>
  );
};
export default PWAInstallNotice;
