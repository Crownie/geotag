import React, {FunctionComponent, useCallback, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  coordinatesUpdated,
  fetchLocation,
  selectCurrentLocation,
} from '../features/location/location.slice';
import Header from '../common/components/Header';
import {Box, Container} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import LatLngInputs from '../features/location/components/LatLngInputs';
import Map from '../features/location/components/Map';
import FooterTab from '../common/components/FooterTab';
import {Favorite, GpsFixed} from '@material-ui/icons';
import {useModal} from 'react-modal-helper';
import MyModalAdapter from '../common/components/MyModalAdapter';
import SaveLocationModal from '../features/location/components/SaveLocationModal';

interface Props {}

const HomePage: FunctionComponent<Props> = () => {
  const position = useSelector(selectCurrentLocation);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const handleGetLocation = useCallback(async () => {
    try {
      setLoading(true);
      await dispatch(fetchLocation());
    } catch (e) {
      console.log(e);
    }
    setLoading(false);
  }, [dispatch]);

  const handleMapChange = useCallback(
    (coords) => {
      dispatch(coordinatesUpdated(coords));
    },
    [dispatch],
  );

  const {open, ModalRenderer} = useModal(MyModalAdapter);

  const handleSave = useCallback(() => {
    open(<SaveLocationModal />);
  }, [open]);

  return (
    <>
      <ModalRenderer />
      <Header
        rightContent={
          <Button color="inherit" onClick={handleSave} startIcon={<Favorite />}>
            Save
          </Button>
        }
      />
      <Container
        maxWidth="sm"
        className="App"
        style={{flex: 1, overflow: 'auto'}}
      >
        <Box pb={10} />
        <Button
          onClick={handleGetLocation}
          variant="contained"
          color="primary"
          size="large"
          startIcon={<GpsFixed />}
        >
          Get coordinates
        </Button>
        <Box pb={5} />

        {loading && <p>loading...</p>}
        <LatLngInputs />
        <Box pb={5} />

        {position && (
          <Map
            latitude={position.coords.latitude}
            longitude={position.coords.longitude}
            onChange={handleMapChange}
          />
        )}
      </Container>
      <FooterTab />
    </>
  );
};

export default HomePage;
