import React, {FunctionComponent} from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import HomePage from './HomePage';
import SavedPage from './SavedPage';
import {logPageView} from '../common/utils/analytics';

interface Props {}

const Routes: FunctionComponent<Props> = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route exact path="/saved">
          <SavedPage />
        </Route>
      </Switch>
      {/*Render for all route*/}
      <Route
        component={() => {
          logPageView();
          return null;
        }}
      />
    </Router>
  );
};

export default Routes;
