import React, {FunctionComponent, useCallback, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import Header from '../common/components/Header';
import {
  Avatar,
  Box,
  Button,
  Container,
  IconButton,
  InputAdornment,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Paper,
  TextField,
} from '@material-ui/core';
import FooterTab from '../common/components/FooterTab';
import {
  Location,
  locationDeleted,
  selectFilterLocations,
} from '../features/location/location.slice';
import {Delete, LocationOn, Search} from '@material-ui/icons';
import {useModal} from 'react-modal-helper';
import MyModalAdapter from '../common/components/MyModalAdapter';
import LocationViewModal from '../features/location/components/LocationViewModal';
import {toast} from '../common/components/Toast';
import ConfirmModal from '../common/components/AlertModal/ConfirmModal';
import ExportCsvModal from '../features/location/components/ExportCsvModal';

interface Props {}

const SavedPage: FunctionComponent<Props> = () => {
  const dispatch = useDispatch();
  const [name, setName] = useState<string>('');
  const locations = useSelector(selectFilterLocations(name));
  const {open, ModalRenderer} = useModal(MyModalAdapter);

  const handleItemClick = useCallback(
    (location: Location) => {
      open(<LocationViewModal location={location} />);
    },
    [open],
  );

  const handleDelete = useCallback(
    (location: Location, event) => {
      event.stopPropagation();
      if (location.id) {
        open(
          <ConfirmModal
            title="Are you sure?"
            message={`You want to delete '${location.name}'`}
            onConfirm={() => {
              dispatch(locationDeleted(location.id!));
              toast(
                'success',
                `Location '${location.name}' was deleted successfully`,
              );
            }}
          />,
        );
      }
    },
    [dispatch, open],
  );

  const handleExportCSV = (event: React.MouseEvent<HTMLButtonElement>) => {
    open(<ExportCsvModal />);
  };

  return (
    <>
      <ModalRenderer />
      <Header
        rightContent={
          <Button color="inherit" onClick={handleExportCSV}>
            Export CSV
          </Button>
        }
      />

      <Container
        maxWidth="sm"
        className="App"
        style={{flex: 1, overflow: 'auto'}}
      >
        <Box pb={2} />
        <TextField
          variant="outlined"
          onChange={(e) => setName(e.target.value)}
          fullWidth
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <Search />
              </InputAdornment>
            ),
          }}
        />
        <Box pb={1} />
        <div>
          <List component="div">
            {locations.map((location) => {
              const {latitude, longitude} = location.position.coords;
              const key = latitude + '' + longitude;
              return (
                <React.Fragment key={key}>
                  <ListItem
                    component={Paper}
                    style={{marginBottom: 16, borderRadius: 7}}
                    button
                    onClick={handleItemClick.bind(null, location)}
                  >
                    <ListItemAvatar>
                      <Avatar>
                        <LocationOn />
                      </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                      primary={location.name}
                      secondary={`${latitude}, ${longitude}`}
                    />
                    <IconButton onClick={handleDelete.bind(null, location)}>
                      <Delete />
                    </IconButton>
                  </ListItem>
                </React.Fragment>
              );
            })}
          </List>
        </div>
      </Container>
      <FooterTab />
    </>
  );
};
export default SavedPage;
