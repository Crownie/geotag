import React, {FunctionComponent, ReactNode} from 'react';
import {
  AppBar,
  Box,
  Divider,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography,
} from '@material-ui/core';
import {Favorite, Home, Menu} from '@material-ui/icons';
import DrawerMenu, {useDrawerMenu} from './DrawerMenu';
import {useHistory} from 'react-router-dom';
import PWAInstallNotice from '../../features/pwa/PWAInstallNotice';
import FillSpace from './FillSpace';

interface Props {
  rightContent?: ReactNode;
}

const Header: FunctionComponent<Props> = ({rightContent}) => {
  const history = useHistory();
  const {toggleDrawer} = useDrawerMenu();
  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={toggleDrawer.bind(null, 'left-drawer', true)}
          >
            <Menu />
          </IconButton>
          <Typography variant="h6" style={{flex: 1}}>
            GeoTag
          </Typography>
          {rightContent}
        </Toolbar>
      </AppBar>
      <DrawerMenu id="left-drawer" anchor="left">
        <Box px={2} py={3} />
        <List>
          <ListItem
            onClick={() => {
              history.replace('/');
              toggleDrawer('left-drawer', false);
            }}
            button
          >
            <ListItemIcon>
              <Home />
            </ListItemIcon>
            <ListItemText primary="Home" />
          </ListItem>
          <ListItem
            onClick={() => {
              history.replace('/saved');
              toggleDrawer('left-drawer', false);
            }}
            button
          >
            <ListItemIcon>
              <Favorite />
            </ListItemIcon>
            <ListItemText primary="Saved Locations" />
          </ListItem>
        </List>
        <FillSpace />
        <Divider />
        <PWAInstallNotice />
      </DrawerMenu>
    </>
  );
};

export default Header;
