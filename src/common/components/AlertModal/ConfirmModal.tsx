import React, {FunctionComponent, useCallback, useEffect, useRef, useState,} from 'react';
import styled from 'styled-components';
import {Button, Grid, Typography} from '@material-ui/core';
import WarningIcon from './sweet-alert-icons/WarningIcon';
import {Box} from '../Box';
import LoadingButton from '../LoadingButton';
import {useModalContext} from 'react-modal-helper';

interface Props {
  title: string;
  message: string;
  onConfirm: () => boolean | void | Promise<boolean>;
}

const ConfirmModal: FunctionComponent<Props> = ({
                                                  title,
                                                  message,
                                                  onConfirm,
                                                }) => {
  const isMounted = useRef(true);
  const {close} = useModalContext();
  const [loading, setLoading] = useState<boolean>(false);

  const handleConfirm = useCallback(async () => {
    setLoading(true);
    const res = await onConfirm();
    if (res === true || res === undefined) {
      close();
    }
    if (isMounted) {
      setLoading(false);
    }
  }, [close, onConfirm]);

  useEffect(() => {
    return () => {
      isMounted.current = false;
    };
  }, []);

  return (
    <div>
      <Box direction="column" alignItems="center" pv={3} ph={3}>
        <WarningIcon/>
        <Title$ variant="h5">{title}</Title$>
        <Message$>{message}</Message$>
        <Grid container spacing={2} justify="center">
          <Grid xs={6} item>
            <LoadingButton
              fullWidth
              variant="contained"
              color="primary"
              onClick={handleConfirm}
              loading={loading}
              loadingText="Please wait..."
            >
              Yes
            </LoadingButton>
          </Grid>
          <Grid xs={6} item>
            <Button
              fullWidth
              variant="contained"
              onClick={() => close()}
              disabled={loading}
            >
              No
            </Button>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
};

const Title$ = styled(Typography)`
  margin-top: 16px;
  margin-bottom: 16px;
  text-align: center;
`;

const Message$ = styled(Typography)`
  margin-bottom: 16px;
  text-align: center;
`;

export default ConfirmModal;
