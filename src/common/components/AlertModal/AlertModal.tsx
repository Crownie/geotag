import React, {FunctionComponent} from 'react';
import styled from 'styled-components';
import {Button, Typography} from '@material-ui/core';
import WarningIcon from './sweet-alert-icons/WarningIcon';
import SuccessIcon from './sweet-alert-icons/SuccessIcon';
import ErrorIcon from './sweet-alert-icons/ErrorIcon';
import {useModalContext} from 'react-modal-helper';

export enum AlertType {
  SUCCESS = 'success',
  WARNING = 'warning',
  ERROR = 'error',
  INFO = 'info',
}

interface Props {
  type?: AlertType;
  title?: string;
  message?: string;
  onClose?: () => void;
}

const AlertModal: FunctionComponent<Props> = ({
                                                type = AlertType.SUCCESS,
                                                title,
                                                message,
                                                onClose,
                                              }) => {
  const {close} = useModalContext();
  let AnimIcon;
  switch (type) {
    case AlertType.SUCCESS:
      AnimIcon = SuccessIcon;
      break;
    case AlertType.WARNING:
      AnimIcon = WarningIcon;
      break;
    case AlertType.ERROR:
      AnimIcon = ErrorIcon;
      break;
  }
  return (
    <Alert$>
      {AnimIcon && <AnimIcon/>}
      <Title$ variant="h4">{title}</Title$>
      <Message$>{message}</Message$>
      <Button
        variant="contained"
        color="primary"
        style={{width: '100%', marginBottom: 16}}
        onClick={() => {
          close();
          onClose && onClose();
        }}
      >
        OK
      </Button>
    </Alert$>
  );
};

const Alert$ = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  align-items: center;
`;

const Title$ = styled(Typography)`
  margin-top: 16px;
  margin-bottom: 16px;
`;

const Message$ = styled(Typography)`
  margin-bottom: 16px;
`;

export default AlertModal;
