import Snackbar from '@material-ui/core/Snackbar';
import React, {SyntheticEvent, useCallback} from 'react';
import MuiAlert from '@material-ui/lab/Alert';
import {AlertProps} from '@material-ui/lab/Alert/Alert';

interface ToastProviderProps {
  children?: any;
}

let toastHandler: any = null;

export const toast = (type: AlertProps['severity'], message: string) => {
  if (!toastHandler) {
    throw new Error(`Ensure that you've wrapped the app with <ToastProvider/>`);
  }
  toastHandler(type, message);
};

export function ToastProvider({children}: ToastProviderProps) {
  const [state, setState] = React.useState<any>({
    open: false,
    type: 'success',
    message: '',
  });

  toastHandler = useCallback(
    (type: AlertProps['severity'], message: string) => {
      setState({type, message, open: true});
    },
    [setState],
  );

  const handleClose = (event?: SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setState({
      ...state,
      open: false,
    });
  };

  return (
    <>
      {children}
      <Snackbar
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={state.open}
        autoHideDuration={2000}
        onClose={handleClose}
      >
        <MuiAlert variant="filled" onClose={handleClose} severity={state.type}>
          {state.message}
        </MuiAlert>
      </Snackbar>
    </>
  );
}
