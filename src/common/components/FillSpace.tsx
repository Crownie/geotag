import React, {FunctionComponent} from 'react';

const FillSpace: FunctionComponent = () => {
  return <div style={{flex: 1}} />;
};

export default FillSpace;
