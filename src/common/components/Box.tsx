import React, {
  CSSProperties,
  FunctionComponent,
  ReactNode,
  useCallback,
} from 'react';
import { useTheme } from 'styled-components';

export interface BoxProps {
  children?: ReactNode;
  pt?: number;
  pl?: number;
  pr?: number;
  pb?: number;
  ph?: number;
  pv?: number;
  p?: number;
  mt?: number;
  ml?: number;
  mr?: number;
  mb?: number;
  mh?: number;
  mv?: number;
  m?: number;
  bg?: string;
  direction?: CSSProperties['flexDirection'];
  alignItems?: CSSProperties['alignItems'];
  justify?: CSSProperties['justifyContent'];
  textAlign?: CSSProperties['textAlign'];
  flexWrap?: CSSProperties['flexWrap'];
  style?: CSSProperties;
  className?: string;
}

export const Box: FunctionComponent<BoxProps> = ({
  children,
  pt,
  pl,
  pr,
  pb,
  ph,
  pv,
  p,
  mt,
  ml,
  mr,
  mb,
  mh,
  mv,
  m,
  bg,
  direction,
  alignItems,
  justify,
  textAlign,
  flexWrap,
  style,
  className,
}) => {
  const theme = useTheme();

  const getSpace = useCallback(
    (...args: (number | undefined)[]) => {
      // find the first that is not undefined
      const v = args.find((i) => i !== undefined);
      return v && theme.spacing(v);
    },
    [theme],
  );

  const _style = {
    display: 'flex',
    flexDirection: direction,
    alignItems,
    flexWrap,
    justifyContent: justify,
    paddingTop: getSpace(pt, pv, p),
    paddingLeft: getSpace(pl, ph, p),
    paddingBottom: getSpace(pb, pv, p),
    paddingRight: getSpace(pr, ph, p),
    marginTop: getSpace(mt, mv, m),
    marginLeft: getSpace(ml, mh, m),
    marginBottom: getSpace(mb, mv, m),
    marginRight: getSpace(mr, mh, m),
    backgroundColor: bg,
    textAlign,
  };
  return (
    <div className={className} style={{ ..._style, ...style }}>
      {children}
    </div>
  );
};
