import React, {
  FunctionComponent,
  useCallback,
  useContext,
  useMemo,
  useState,
} from 'react';
import {SwipeableDrawer, SwipeableDrawerProps} from '@material-ui/core';

interface Props {
  id: string;
  anchor: SwipeableDrawerProps['anchor'];
}

const DrawerMenu: FunctionComponent<Props> = ({id, anchor, children}) => {
  const {toggleDrawer, isOpen} = useContext(DrawerMenuContext);

  return (
    <>
      <SwipeableDrawer
        anchor={anchor}
        open={isOpen(id)}
        onClose={toggleDrawer.bind(null, id, false)}
        onOpen={toggleDrawer.bind(null, id, true)}
      >
        <div
          style={{
            width: 260,
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          {children}
        </div>
      </SwipeableDrawer>
    </>
  );
};

interface DrawerMenuProps {
  toggleDrawer: (id: string, open: boolean) => void;
  isOpen: (id: string) => boolean;
}

const DrawerMenuContext = React.createContext<DrawerMenuProps>({} as any);

export const useDrawerMenu = () => {
  return useContext(DrawerMenuContext);
};

export const DrawerMenuProvider: FunctionComponent = ({children}) => {
  const [openState, setOpenState] = useState<Record<string, boolean>>({});

  const toggleDrawer = useCallback((id: string, open: boolean) => {
    setOpenState((prev) => ({
      ...prev,
      [id]: open,
    }));
  }, []);

  const isOpen = useCallback(
    (id: string): boolean => {
      return !!openState[id];
    },
    [openState],
  );

  const value = useMemo(() => ({toggleDrawer, isOpen}), [toggleDrawer, isOpen]);

  return (
    <DrawerMenuContext.Provider value={value}>
      {children}
    </DrawerMenuContext.Provider>
  );
};

export default DrawerMenu;
