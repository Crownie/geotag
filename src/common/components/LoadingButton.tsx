import Button, { ButtonProps } from '@material-ui/core/Button/Button';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import React from 'react';

interface Props extends ButtonProps {
  loading?: boolean;
  loadingText?: string;
  children?: any;
}

export default function LoadingButton(props: Props) {
  const { loading, loadingText, children, disabled, ...others } = props;

  return (
    <Button {...others} disabled={loading || disabled}>
      {loading && (
        <span style={{ position: 'absolute' }}>
          <CircularProgress size={16} />
          <span style={{ marginLeft: 15 }}>{loadingText}</span>
        </span>
      )}
      <span style={{ opacity: loading ? 0 : 1 }}>{children}</span>
    </Button>
  );
}
