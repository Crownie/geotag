import React, {FunctionComponent, useCallback, useState} from 'react';
import {BottomNavigation, BottomNavigationAction} from '@material-ui/core';
import {Favorite, Home, Settings} from '@material-ui/icons';
import {useHistory} from 'react-router-dom';
import {toast} from './Toast';

interface Props {}

const paths = ['/', '/saved', '/settings'];

const FooterTab: FunctionComponent<Props> = () => {
  const history = useHistory();
  const [, forceRender] = useState({});

  const handleChange = useCallback(
    (event, val) => {
      if (paths[val] === '/settings') {
        toast('info', 'Coming soon...');
      } else {
        history.replace(paths[val]);
        forceRender({});
      }
    },
    [history],
  );

  const value = paths.findIndex((item) => history.location.pathname === item);

  return (
    <BottomNavigation value={value} onChange={handleChange} showLabels>
      <BottomNavigationAction label="Home" icon={<Home />} />
      <BottomNavigationAction label="Saved" icon={<Favorite />} />
      <BottomNavigationAction label="Settings" icon={<Settings />} />
    </BottomNavigation>
  );
};

export default FooterTab;
