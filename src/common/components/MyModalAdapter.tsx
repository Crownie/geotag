import React from 'react';
import {createModalAdapter} from 'react-modal-helper';
import {Dialog} from '@material-ui/core';

const MyModalAdapter = createModalAdapter(({children, isOpen, close}) => {
  return <Dialog open={isOpen} onClose={() => {
    close()
  }} maxWidth="xs" fullWidth>{children}</Dialog>
})

export default MyModalAdapter;
