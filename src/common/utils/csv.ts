export function convertArrayOfObjectsToCSV(
  data: Record<string, string | number | boolean | null | undefined>[],
) {
  let result = '';

  const columnDelimiter = ',';
  const lineDelimiter = '\n';

  const keys = Object.keys(data[0]);

  result = '';
  result += keys.join(columnDelimiter);
  result += lineDelimiter;

  data.forEach(function (item) {
    let ctr = 0;
    keys.forEach(function (key) {
      if (ctr > 0) result += columnDelimiter;

      result += item[key];
      ctr++;
    });
    result += lineDelimiter;
  });

  return result;
}
export function downloadCSV(csv: string, filename: string = 'export.csv') {
  if (csv == null) return;

  filename = filename || 'export.csv';

  if (!csv.match(/^data:text\/csv/i)) {
    csv = 'data:text/csv;charset=utf-8,' + csv;
  }
  const data = encodeURI(csv);

  const link = document.createElement('a');
  link.setAttribute('href', data);
  link.setAttribute('download', filename);
  link.click();
}
