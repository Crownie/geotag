import {GA4React} from 'ga-4-react';

const isLocalhost = Boolean(
  window.location.hostname === 'localhost' ||
    // [::1] is the IPv6 localhost address.
    window.location.hostname === '[::1]' ||
    // 127.0.0.0/8 are considered localhost for IPv4.
    window.location.hostname.match(
      /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/,
    ),
);

const ga4react = new GA4React('G-HH1QR2HJF2', {
  /* ga custom config, optional */
});
const initGA = async () => {
  if (isLocalhost) {
    return;
  }
  if (!GA4React.isInitialized()) {
    await ga4react.initialize();
  }
};

export const logPageView = async () => {
  if (isLocalhost) {
    return;
  }
  await initGA();
  ga4react.pageview(window.location.pathname);
  ga4react.gtag('event', 'pageview', window.location.pathname);
};

export const logEvent = async (name: 'click' | 'select_tab', data: any) => {
  if (isLocalhost) {
    return;
  }
  await initGA();
  ga4react.gtag('event', name, data);
};
