/// <reference types="react-scripts" />
// extend the module declarations using custom theme type
import {Theme} from '@material-ui/core';
import {RootState} from './app/reducer';

declare module '*.json' {
  const value: any;
  export default value;
}

declare module 'styled-components' {
  export interface DefaultTheme extends Theme {}
}

declare module 'react-redux' {
  export interface DefaultRootState extends RootState {}
}
